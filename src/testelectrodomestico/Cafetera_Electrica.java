/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testelectrodomestico;

import java.util.Scanner;

/**
 *
 * @author Usuario
 */
public class Cafetera_Electrica extends Electrodomestico{
    
    private int minutos = 0;
    private int on =0 ;
    private int off = 0;
    Scanner leer = new Scanner(System.in);

    public Cafetera_Electrica(String nombre, String desc, String marca, String color, int cantidad, double precio) {
        super(nombre, desc, marca, color, cantidad, precio);
    }

    public Cafetera_Electrica() {
    }
    
    

    public int getMinutos() {
        return minutos;
    }

    public void setMinutos(int minutos) {
        this.minutos = minutos;
    }

    public int getOn() {
        return on;
    }

    public void set(int on) {
        this.on = on;
    }

    public int getOff() {
        return off;
    }

    public void setOff(int off) {
        this.off = off;
    }
    
    
    public void prender(){
        do{
        System.out.println("-Para encender la cafetera presione 1 ");
        on=leer.nextInt();
        
        if(on ==1){
            System.out.println("Cafereta Encendida");
            
        }else{
            System.out.println("No pasa nada...");            
        }
        }while(on!=1);
    }
    
    
    public void calentar(){     
        do{
        System.out.println("El tiempo establecido para el uso de esta cafetera es de 1 minuto hasta 10 minutos");
        System.out.println("Ingrese el tiempo para calentar su bebida");
        minutos=leer.nextInt();
        if(minutos<1 || minutos>=10){
            System.out.println("Recuerde que el tiempo minimo de uso es de 1 minuto y máximo de 10 minutos");
        }else{
            System.out.println("Su producto está terminado");        
        }
        }while(minutos<1 || minutos>=10);
    }
    
    public void apagar(){
        do{
        System.out.println("-Para apagar la cafetera presione 1 "+"\n"+"- Para degustar otro café, pulse cualquier otro número");
        off=leer.nextInt();
        
        if(off ==1){
            System.out.println("Cafereta Apagada");
            
        }else{
            calentar();            
        }
        }while(off!=1);
    }
}
