/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testelectrodomestico;

/**
 *
 * @author Usuario
 */
public abstract class Electrodomestico {

    public Electrodomestico(String nombre, String desc, String marca, String color, int cantidad, double precio) {
        this.nombre = nombre;
        this.desc = desc;
        this.cantidad = cantidad;
        this.precio = precio;
        this.marca = marca;
        this.color = color;
    }

    public Electrodomestico() {
    }
    
    
    private String nombre, desc, marca, color;
    private int cantidad;
    private double precio;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
    
    
    
    public String toString(){
        return "Electrodoméstico:"+ nombre+"\n"
        +"Descripción: "+desc+"\n"
        +"Marca: "+marca+"\n"
        +"Color: "+color+"\n"
        +"cantidad: "+cantidad+"\n"
        +"Precio: "+precio+"\n";
    }
}

